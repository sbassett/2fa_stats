
import csv
import requests
# Find position of the admin list for each wiki
# File is in format: [wikiname]\nuser_name\n...
admin_pos = []
with open('admins_in_wikis','r') as f:
    pos = 0
    for line in f:
        if line.strip() == 'user_name':
            wiki = prev_line
            admin_pos.append(pos+1)
        pos += 1
        prev_line = line.strip()

# Go back and store list of admins for each wiki
wiki_admin_list = {}
with open('admins_in_wikis','r') as f:
    pos = 0
    wiki_index = -1
    for line in f:
        if line.strip() == 'user_name':
            wiki = prev_line
            wiki_index += 1
        store_admin = False
        if wiki_index > 0 and wiki_index < len(admin_pos)-1:
            if pos >= admin_pos[wiki_index] and pos < admin_pos[wiki_index+1]-2:
                store_admin = True
        if wiki_index == len(admin_pos) - 1 and pos >= admin_pos[wiki_index]:
            store_admin = True
        if store_admin:
            admin_list = wiki_admin_list.get(wiki, [])
            admin_list.append(line.strip())
            wiki_admin_list[wiki] = admin_list
        pos += 1
        prev_line = line.strip()

# Store list of 2fa enabled users
twofa_admin_list = []
with open('twofa_enabled_users','r') as f:
    for line in f:
        twofa_admin_list.append(line.strip())
with open('twofa_testers','r') as f:
    for line in f:
        twofa_admin_list.append(line.strip())

# write csv of percentage 2fa users enabled per wiki
with open('2fastats.csv', 'w', newline='') as csvfile:
    statswriter = csv.writer(csvfile)
    statswriter.writerow(['wiki','num_2fa_admins','num_admins'])
    for wiki, admin_list in wiki_admin_list.items():
        count_2fa = 0
        for admin in admin_list:
            if admin in twofa_admin_list:
                count_2fa += 1
        statswriter.writerow([wiki, count_2fa, len(admin_list)])

# following obtained from 'wmgPrivilegedGroups' for production wikis
privileged_groups = [
#// core or extension groups
'bureaucrat', 'checkuser', 'interface-admin', 'oauthadmin', 'suppress', 'sysop',
#// custom groups used on several wikis
'arbcom', 'botadmin', 'eliminator', 'import', 'interface-editor', 'transwiki',
#// custom groups used on one or a few wikis
'abusefilter' #/* enwiki */
,'curator' #/* enwikiversity */
, 'engineer' #/* cswiki, ruwiki */
, 'facilitator' #/* frwikinews */
, 'founder' #/* enwiki */
, 'templateeditor' #/* rowiki */
, 'test-sysop' #/* betawikiversity, incubatorwiki */
, 'translator' #/* incubatorwiki */
, 'wikidata-staff', 'wikifunctions-staff',
#// metawiki local groups with global powers (some also on testwiki)
'centralnoticeadmin', 'global-renamer', 'translationadmin', 'wmf-officeit', 'wmf-supportsafety',
#// wikitech local groups with global powers
'oathauth',
]
api_urls = {
    'enwiki': 'en.wikipedia.org',
    'commonswiki': 'commons.wikimedia.org',
    'dewiki': 'de.wikipedia.org',
    'frwiki': 'fr.wikipedia.org',
    'mediawikiwiki': 'www.mediawiki.org',
    'itwiki': 'it.wikipedia.org',
    'plwiki': 'pl.wikipedia.org',
    'metawiki': 'meta.wikimedia.org',
    'wikidatawiki': 'www.wikidata.org'
}
MAX_GROUP_COUNT = 50

# get user group 2fa enabled for enwiki
with open('2fagroupstats.csv', 'w', newline='') as csvfile:
    for wiki in api_urls.keys():
        statswriter = csv.writer(csvfile)
        statswriter.writerow(['wiki','group','num_2fa_admins','num_admins'])
        admin_list = wiki_admin_list[wiki]
        # get user group memberships (group => user_list)
        group_membership = {}
        for index in range(0, len(admin_list), MAX_GROUP_COUNT):
            users = admin_list[index:index+MAX_GROUP_COUNT]
            S = requests.Session()
            URL = "https://%s/w/api.php" % api_urls[wiki]

            PARAMS = {
                "action": "query",
                "format": "json",
                "list": "users",
                "ususers": '|'.join(users),
                "usprop": "blockinfo|groups|editcount|registration|emailable|gender"
            }

            R = S.get(url=URL, params=PARAMS)
            DATA = R.json()

            USERS = DATA["query"]["users"]

            for u in USERS:
                groups = [value for value in u["groups"] if value in privileged_groups]
                username = u["name"]
                for group in groups:
                    user_list = group_membership.get(group, [])
                    user_list.append(username)
                    group_membership[group] = user_list
        for group, user_list in group_membership.items():
            count_2fa = 0
            for user in user_list:
                if user in twofa_admin_list:
                    count_2fa += 1
            statswriter.writerow([wiki, group, count_2fa, len(user_list)])


